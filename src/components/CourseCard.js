// Base Imports
import React, {useState, useEffect} from 'react';

// dependencies
// import React from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';




export default function CourseCard(props){
    let course = props.course;

    const [ isDisabled, setIsDisabled ] = useState(0);
    const [ seats, setSeats ] = useState(30);

    useEffect(()=> {
        if (seats === 0) {
            setIsDisabled(true);
        }
    },[seats]);


    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>{course.price}</p>
                <h6>Seats</h6>
                <p>{seats} remaining</p>
                <Button variant="primary" onClick={() => setSeats(seats - 1)} disabled={isDisabled}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
