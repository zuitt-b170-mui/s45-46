/*
  after installing, remove all files inside the src folder except index.js
  remove the README.md file in the root folder of the app
*/
// import dependencies
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import App from './App'
/*
  install bootstrap and react bootstrap first: npm install bootstrap@4.6.0 react-bootstrap@1.5.2
*/

// Bootstrap Dependencies
// import 'bootstrap/dist/css/bootstrap.min.css';
// import './index.css'

ReactDOM.render(<App/>,document.getElementById('root'));
