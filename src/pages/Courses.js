// Base imports
import React from 'react';
import Course from '../components/CourseCard'

// Bootstrap dependencies
import Container from 'react-bootstrap/Container';

// Data Import
import courses from '../mock-data/courses';

export default function Courses(){
    const CourseCards = courses.map((courses) => {
        return (
            <Course course={courses}/>
        )
    })
    return(
        <Container fluid>
            {CourseCards}
        </Container>
    )
}